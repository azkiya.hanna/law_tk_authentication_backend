from django.db import models

class UserModel(models.Model):
    id = models.AutoField(primary_key=True)
    username = models.TextField()
    email = models.TextField()
    access_token= models.TextField()
    def __str__(self):
		    return self.username

