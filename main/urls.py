from django.urls import path
from . import views

app_name = 'main'

urlpatterns = [
    path('', views.index, name='index'),
    path('add-user/', views.add_user, name='add-user'),
    path('get-user-authorization/',views.get_authorization, name='get-authorization'),
    path('log-out/',views.log_out, name='log-out')

]