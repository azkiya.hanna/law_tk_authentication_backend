from django.http import JsonResponse
from rest_framework import status
from rest_framework.decorators import api_view
from main.serializer import UserModelSerializer
from django.views.decorators.csrf import csrf_exempt
from .models import *
from django.core.exceptions import ObjectDoesNotExist
import requests
import logging
import logstash

logger = logging.getLogger("logstash")
logger.setLevel(logging.INFO)        
handler = logstash.LogstashHandler(
    'bcdf449f-5185-4ea8-802d-7eb4d09a8527-ls.logit.io',
    19001,
    version=1
)
logger.addHandler(handler)
client_id ="541656281005-upj385kcj74ag2jvuo28pm2grl8k4m66.apps.googleusercontent.com"

def index(request):
    return JsonResponse({'message':'this is law text filtering authentication backend'})

def validateRequest(request, listOfAttrs):
    client_id_request = str(request.data.get('client_id'))
    if client_id_request != client_id:
        return JsonResponse({'error': 'client Id is not valid'}, status=status.HTTP_400_BAD_REQUEST)
    for atr in listOfAttrs:
        data = request.data.get(atr)
        if data is None:
            return JsonResponse({'error': "{} is required".format(atr)}, status=status.HTTP_400_BAD_REQUEST)
    return True
def get_credential(access_token):
    credential_api = "https://www.googleapis.com/oauth2/v2/userinfo?scope=openid%20profile%20email"
    headers =  {"Authorization":"Bearer " + access_token}
    response = requests.get(credential_api,headers=headers)
    return response


@api_view(['POST'])
@csrf_exempt
def add_user(request):
    isRequestValid = validateRequest(request,['access_token'])
    if isRequestValid != True:
        return isRequestValid
    access_token = request.data.get('access_token')
    response = get_credential(access_token)
    if(response.status_code == 401):
        logger.info('Unathorized user is detected')
        return JsonResponse({'message': 'Unauthorized user / token has expired'}, status=status.HTTP_401_UNAUTHORIZED)   
    response = response.json()
    email = response["email"]
    try: 
        user = UserModel.objects.get(email=email)
        user.access_token = access_token
        user.save()
        logger.info('recently saved user has login')
        return JsonResponse({'email': response['email'], 'username': response['name']},status = status.HTTP_200_OK,safe=False)
    except ObjectDoesNotExist:
        
        username = response['name']
        new_user = UserModel.objects.create(
            username = username,
            email = email,
            access_token = access_token
        )
        new_user = UserModelSerializer(new_user)
        logger.info('new user has saved')
        return JsonResponse(new_user.data,status=status.HTTP_201_CREATED,safe=False)
    except:
        return JsonResponse({'error': 'bad request'}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
@csrf_exempt
def get_authorization(request):
    isRequestValid = validateRequest(request,['access_token'])
    if isRequestValid != True:
        return isRequestValid
    access_token = request.data.get('access_token')
    response = get_credential(access_token)
    if(response.status_code == 401):
        logger.info('Unathorized user is detected')
        return JsonResponse({'message': 'Unauthorized user / token has expired'}, status=status.HTTP_401_UNAUTHORIZED)   
    try: 
        response = response.json()
        email = response["email"]  
        UserModel.objects.get(email=email)
        logger.info('User with email: ' + response['email'] + ' is authorized')
        return JsonResponse({'email': response['email'], 'username': response['name']},status = status.HTTP_200_OK,safe=False)
    except ObjectDoesNotExist:
        logger.warning('UNREGISTERED USER DETECTED')
        return JsonResponse({'message': 'User is not registered'}, status=status.HTTP_401_UNAUTHORIZED)

@api_view(['POST'])
@csrf_exempt
def log_out(request):
    print(request.data)
    isRequestValid = validateRequest(request,['access_token','email'])
    if isRequestValid != True:
        return isRequestValid
    access_token = request.data.get('access_token')
    email = request.data.get('email')
    try: 
        user = UserModel.objects.get(email=email)
    except ObjectDoesNotExist:
        return JsonResponse({'message': 'User can not be found'},status = status.HTTP_200_OK)
    revoke_api = "https://accounts.google.com/o/oauth2/revoke"
    response = requests.get(revoke_api,params={'token': access_token})
    if(response.status_code == 200):
        user.delete()
        logger.info('User with email: ' + email + ' logged out success')
        return JsonResponse({'message': 'Log out success'},status = status.HTTP_200_OK)
    else:
        logger.error('User with email: ' + email + 'logged out failed')
        return JsonResponse({'message': 'Log out failed'},status=status.HTTP_401_UNAUTHORIZED)
        

